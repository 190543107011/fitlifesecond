package com.example.fitlife.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.fitlife.Models.GymSubCategoryModel;
import com.example.fitlife.R;
import com.example.fitlife.adapter.GymSubCategoryAdapter;
import com.example.fitlife.database.GymSubCategoryDb;

import java.util.ArrayList;

public class GymSubCategoryActivity extends AppCompatActivity {

    GymSubCategoryAdapter gymSubCategoryAdapter;

    GymSubCategoryDb gymSubCategoryDb;

    ArrayList<GymSubCategoryModel> subcategorylist;

    Intent intent;

    int categoryId;
    String categoryName;

    RecyclerView activity_gym_subcategory_recycler_view;

    CardView chest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym_sub_category);
        init();
        process();
        listener();
    }

    public void init() {

        intent = getIntent();

        categoryId = intent.getIntExtra("CategoryId", 2);
        categoryName = intent.getStringExtra("CategoryName");

        //Toast.makeText(this, "Id = " + categoryId, Toast.LENGTH_SHORT).show();

        activity_gym_subcategory_recycler_view = findViewById(R.id.activity_gym_subcategory_recycler_view);
        chest = findViewById(R.id.chest);

        subcategorylist = new ArrayList<>();
        gymSubCategoryDb = new GymSubCategoryDb(this);
        subcategorylist = gymSubCategoryDb.all_sub_category_list(categoryId);

        //Toast.makeText(this, "SubCategory List Is " + subcategorylist.size(), Toast.LENGTH_SHORT).show();

    }

    public void process() {

        gymSubCategoryAdapter = new GymSubCategoryAdapter(subcategorylist, this, new GymSubCategoryAdapter.ClickListeners() {
            @Override
            public void onViewClick(int position) {

                int tempSubCategoryId = subcategorylist.get(position).getSubCategoryId();
                String tempSubCategoryName = subcategorylist.get(position).getSubCategoryName();

             Intent intent = new Intent(GymSubCategoryActivity.this, Gym_Exercises_Activity.class);
                intent.putExtra("SubCategoryId",tempSubCategoryId);
                intent.putExtra("SubCategoryName",tempSubCategoryName);
                startActivity(intent);


            }
        });

        activity_gym_subcategory_recycler_view.setLayoutManager(new GridLayoutManager(this, 2));

        activity_gym_subcategory_recycler_view.setAdapter(gymSubCategoryAdapter);

    }

    public void listener() {

    }
}