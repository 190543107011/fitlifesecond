package com.example.fitlife.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.fitlife.Models.GymExercisesListModel;
import com.example.fitlife.R;
import com.example.fitlife.adapter.GymExerciseslistAdapter;
import com.example.fitlife.database.Gym_Exerciseslist;

import java.util.ArrayList;

public class Gym_Exercises_Activity extends AppCompatActivity {

    GymExerciseslistAdapter gymExerciseslistAdapter;

    Gym_Exerciseslist gym_exerciseslist;

    ArrayList<GymExercisesListModel> exerciseslist = new ArrayList<>();

    Intent intent;

    int subcategoryId;
    String subcategoryName;

    CardView pushps;
    RecyclerView activity_gym_exercises_recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym__exercises_);
        init();
        process();
        listener();
    }

    public void init(){
        intent = getIntent();

        subcategoryId = intent.getIntExtra("SubCategoryId", 1);
        subcategoryName = intent.getStringExtra("SubCategoryName");



        activity_gym_exercises_recycler_view = findViewById(R.id.activity_gym_exercises_recycler_view);
        exerciseslist = new ArrayList<>();
        gym_exerciseslist = new Gym_Exerciseslist(this);
        exerciseslist = gym_exerciseslist.all_exercise_list(subcategoryId);
        pushps = findViewById(R.id.pushps);

    }
    public  void process(){

        gymExerciseslistAdapter = new GymExerciseslistAdapter(exerciseslist, this, new GymExerciseslistAdapter.ClickListeners() {
            @Override
            public void onViewClick(int position) {

                int tempSubCategoryId = exerciseslist.get(position).getExerciseslistId();
                String tempSubCategoryName = exerciseslist.get(position).getExercisesName();

                Intent intent = new Intent(Gym_Exercises_Activity.this, exercise_detail_activity.class);
                intent.putExtra("ExerciseslistId",tempSubCategoryId);
                intent.putExtra("ExercisesName",tempSubCategoryName);

                startActivity(intent);



            }
        });
        activity_gym_exercises_recycler_view.setLayoutManager(new GridLayoutManager(this,2));
        activity_gym_exercises_recycler_view.setAdapter(gymExerciseslistAdapter);


    }
    public void listener(){


    }

}