package com.example.fitlife.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.fitlife.Models.DieatSubcategoryModel;
import com.example.fitlife.R;
import com.example.fitlife.adapter.DieatSubCategoryAdapter;
import com.example.fitlife.adapter.GymSubCategoryAdapter;
import com.example.fitlife.database.DieatSubCategory;

import java.util.ArrayList;

public class Dieat_SubCategory_Activity extends AppCompatActivity {


    DieatSubCategoryAdapter dieatSubCategoryAdapter;
    DieatSubCategory dieatSubCategory;

    ArrayList<DieatSubcategoryModel>dieatsubcategorylist;
    Intent intent;

    CardView dieatsubcategory;

    int DieatId;
    String DieatName;

    RecyclerView activity_dieat_subcategory_recycler_view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dieat__sub_category_);
        init();

        process();
    }

    public  void init(){

        intent = getIntent();
        DieatId= intent.getIntExtra("DieatId", 2);
        DieatName = intent.getStringExtra("DieatName");

        activity_dieat_subcategory_recycler_view = findViewById(R.id.activity_dieat_subcategory_recycler_view);

        dieatsubcategory = findViewById(R.id.dieatsubcategory);

        dieatsubcategorylist =new ArrayList<>();
        dieatSubCategory =new DieatSubCategory(this);
        dieatsubcategorylist = dieatSubCategory.all_dieatsubcategory_list(DieatId);
    }

    public  void process(){


        dieatSubCategoryAdapter = new DieatSubCategoryAdapter(dieatsubcategorylist, this, new DieatSubCategoryAdapter.ClickListeners() {
            @Override
            public void onViewClick(int position) {

                int tempCategoryId = dieatsubcategorylist.get(position).getDishId();
                String tempCategoryName = dieatsubcategorylist.get(position).getDishName();



                Intent intent = new Intent(Dieat_SubCategory_Activity.this, DieatDetailActivity.class);
                intent.putExtra("DishId",tempCategoryId);
                intent.putExtra("DishName",tempCategoryName);

                startActivity(intent);


            }
        });

        activity_dieat_subcategory_recycler_view.setLayoutManager(new GridLayoutManager(this, 2));

        activity_dieat_subcategory_recycler_view.setAdapter(dieatSubCategoryAdapter);
    }
}