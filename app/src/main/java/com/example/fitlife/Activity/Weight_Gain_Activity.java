package com.example.fitlife.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.fitlife.Models.GymSubCategoryModel;
import com.example.fitlife.Models.WeightGainModel;
import com.example.fitlife.Models.WeightloseModel;
import com.example.fitlife.R;
import com.example.fitlife.adapter.WeightGainAdapter;
import com.example.fitlife.adapter.WeightloseAdapter;
import com.example.fitlife.database.GymSubCategoryDb;
import com.example.fitlife.database.WeightGain;
import com.example.fitlife.database.Weightlose;
import com.example.fitlife.example_activity;

import java.util.ArrayList;

public class Weight_Gain_Activity extends AppCompatActivity {


WeightGainAdapter weightGainAdapter;

    WeightGain weightGain;

    ArrayList<WeightGainModel> weekdaylist;
    ArrayList<GymSubCategoryModel> subCategorylist;

    GymSubCategoryDb subCategoryDb;

    Intent intent;

    int categoryId;
    String categoryName;

    RecyclerView activity_weightgain_category_recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight__gain_);
        init();
        process();
    }

    public void init() {

        intent = getIntent();

        categoryId = intent.getIntExtra("CategoryId", 2);
        categoryName = intent.getStringExtra("CategoryName");

        activity_weightgain_category_recycler_view = findViewById(R.id.activity_weightgain_category_recycler_view);


        subCategorylist = new ArrayList<>();
        weekdaylist = new ArrayList<>();
        weightGain = new WeightGain(this);
        weekdaylist = weightGain.all_gain_exercises(categoryId);
        subCategoryDb = new GymSubCategoryDb(this);
        subCategorylist = subCategoryDb.all_sub_category_list(categoryId);

    }

    public void process() {

        // gymSubCategoryAdapter = new GymSubCategoryAdapter(subcategorylist, this);
        weightGainAdapter = new WeightGainAdapter(subCategorylist,weekdaylist, this, new WeightGainAdapter.ClickListeners() {
            @Override
            public void onViewClick(int position) {



                int tempSubCategoryId = subCategorylist.get(position).getSubCategoryId();
                String tempSubCategoryName = subCategorylist.get(position).getSubCategoryName();

                Intent intent = new Intent(Weight_Gain_Activity.this, Gym_Exercises_Activity.class);
                intent.putExtra("SubCategoryId",tempSubCategoryId);
                intent.putExtra("SubCategoryName",tempSubCategoryName);
                startActivity(intent);

            }
        });

        activity_weightgain_category_recycler_view.setLayoutManager(new GridLayoutManager(this, 1));

        activity_weightgain_category_recycler_view.setAdapter(weightGainAdapter);


    }

}