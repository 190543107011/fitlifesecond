package com.example.fitlife.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.fitlife.Models.DaysCategoryModel;
import com.example.fitlife.Models.GymSubCategoryModel;
import com.example.fitlife.R;
import com.example.fitlife.adapter.Bulk_Body_Adapter;
import com.example.fitlife.database.DaysCategorys;
import com.example.fitlife.database.GymSubCategoryDb;

import java.util.ArrayList;

public class Bulk_Body_Activity extends AppCompatActivity {

    RecyclerView activity_gym_bulkbody_recycler_view;
    ArrayList<GymSubCategoryModel> subCategorylist;

    GymSubCategoryDb subCategoryDb;

    ArrayList<DaysCategoryModel> dayslist;
    ArrayList<DaysCategoryModel> dayslist1;
    ArrayList<DaysCategoryModel> dayslist2;
    DaysCategorys daysCategorys;
    Bulk_Body_Adapter bulk_body_adapter;

    Intent intent;

    int categoryId;
    String categoryName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulk__body_);
        init();
        process();
        listener();
    }

    public void init(){

        activity_gym_bulkbody_recycler_view =findViewById(R.id.activity_gym_bulkbody_recycler_view);

        intent = getIntent();

        categoryId = intent.getIntExtra("CategoryId", 2);
        categoryName = intent.getStringExtra("CategoryName");

        subCategorylist = new ArrayList<>();
        subCategoryDb = new GymSubCategoryDb(this);
        subCategorylist = subCategoryDb.all_sub_category_list(categoryId);


        dayslist = new ArrayList<>();
        dayslist1 = new ArrayList<>();
        dayslist2 = new ArrayList<>();
        daysCategorys = new DaysCategorys(this);
        dayslist = daysCategorys.all_day_category(categoryId);

        generateTempList();

    }

    private void generateTempList() {

        for (int i = 0; i < dayslist.size(); i++) {
            DaysCategoryModel daysCategoryModel = dayslist.get(i);
            if ((i % 2) == 0) {
                dayslist1.add(daysCategoryModel);
            } else {
                dayslist2.add(daysCategoryModel);
            }
        }

    }


    public  void process(){

        bulk_body_adapter = new Bulk_Body_Adapter(subCategorylist, dayslist1, dayslist2, this, new Bulk_Body_Adapter.ClickListeners() {
            @Override
            public void onViewClick(int subcategoryid, String subcategoryname) {

               /*int tempSubCategoryId = subCategorylist.get(position).getSubCategoryId();
               String tempSubCategoryName = subCategorylist.get(position).getSubCategoryName();*/

                Intent intent = new Intent(Bulk_Body_Activity.this, Gym_Exercises_Activity.class);
                intent.putExtra("SubCategoryId", subcategoryid);
                intent.putExtra("SubCategoryName", subcategoryname);
                startActivity(intent);

            }
        });

        activity_gym_bulkbody_recycler_view.setLayoutManager(new GridLayoutManager(this, 1));
        activity_gym_bulkbody_recycler_view.setAdapter(bulk_body_adapter);

    }

    public void listener(){

    }

}