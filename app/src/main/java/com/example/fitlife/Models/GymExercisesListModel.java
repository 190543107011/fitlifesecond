package com.example.fitlife.Models;

import java.io.Serializable;

public class GymExercisesListModel implements Serializable {

    int ExerciseslistId;
    int CategoryId;
    int SubCategoryId;
    int IsFavorite;

    public int getIsFavorite() {
        return IsFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    public int getExerciseslistId() {
        return ExerciseslistId;
    }

    public void setExerciseslistId(int exerciseslistId) {
        ExerciseslistId = exerciseslistId;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public int getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        SubCategoryId = subCategoryId;
    }

    public String getExercisesName() {
        return ExercisesName;
    }

    public void setExercisesName(String exercisesName) {
        ExercisesName = exercisesName;
    }

    public String getExercisesImage() {
        return ExercisesImage;
    }

    public void setExercisesImage(String exercisesImage) {
        ExercisesImage = exercisesImage;
    }

    String ExercisesName;
    String ExercisesImage;
}
