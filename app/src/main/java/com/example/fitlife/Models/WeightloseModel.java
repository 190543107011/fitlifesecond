package com.example.fitlife.Models;

import java.io.Serializable;

public class WeightloseModel  implements Serializable {

    int WeekId;
    int SubCategoryId;

    public int getWeekId() {
        return WeekId;
    }

    public void setWeekId(int weekId) {
        WeekId = weekId;
    }

    public int getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        SubCategoryId = subCategoryId;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public String getWeekName() {
        return WeekName;
    }

    public void setWeekName(String weekName) {
        WeekName = weekName;
    }

    int CategoryId;
    String WeekName;

}
