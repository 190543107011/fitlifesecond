package com.example.fitlife.Models;

import java.io.Serializable;

public class WeightGainModel implements Serializable {

    int WeekId;
    int SubCategoryId;
    int CategoryId;

    public int getWeekId() {
        return WeekId;
    }

    public void setWeekId(int weekId) {
        WeekId = weekId;
    }

    public int getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        SubCategoryId = subCategoryId;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public String getWeekName() {
        return WeekName;
    }

    public void setWeekName(String weekName) {
        WeekName = weekName;
    }

    public String getTotalExercises() {
        return TotalExercises;
    }

    public void setTotalExercises(String totalExercises) {
        TotalExercises = totalExercises;
    }

    String WeekName;
    String TotalExercises;
}
