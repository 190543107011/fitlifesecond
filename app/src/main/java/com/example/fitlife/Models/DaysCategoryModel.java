package com.example.fitlife.Models;

import java.io.Serializable;

public class DaysCategoryModel implements Serializable {

    int DaysId;
    int SubCategoryId;
    int CategoryId;

    public int getDaysId() {
        return DaysId;
    }

    public void setDaysId(int daysId) {
        DaysId = daysId;
    }

    public int getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        SubCategoryId = subCategoryId;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public String getDaysName() {
        return DaysName;
    }

    public void setDaysName(String daysName) {
        DaysName = daysName;
    }

    String DaysName;

}
