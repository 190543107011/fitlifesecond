package com.example.fitlife.Models;

import java.io.Serializable;

public class DieatCategoryModel implements Serializable {

    int DieatId;
    String DieatName;
    String DieatImages;

    public int getDieatId() {
        return DieatId;
    }

    public void setDieatId(int dieatId) {
        DieatId = dieatId;
    }

    public String getDieatName() {
        return DieatName;
    }

    public void setDieatName(String dieatName) {
        DieatName = dieatName;
    }

    public String getDieatImages() {
        return DieatImages;
    }

    public void setDieatImages(String dieatImages) {
        DieatImages = dieatImages;
    }
}
