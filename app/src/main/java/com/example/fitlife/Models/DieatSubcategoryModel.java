package com.example.fitlife.Models;

import java.io.Serializable;

public class DieatSubcategoryModel implements Serializable {

    int DishId;
    String DishKcal;

    public String getDishKcal() {
        return DishKcal;
    }

    public void setDishKcal(String dishKcal) {
        DishKcal = dishKcal;
    }

    public int getIsFavorite() {
        return IsFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    int IsFavorite;

    public int getDishId() {
        return DishId;
    }

    public void setDishId(int dishId) {
        DishId = dishId;
    }

    public int getDieatId() {
        return DieatId;
    }

    public void setDieatId(int dieatId) {
        DieatId = dieatId;
    }

    public String getDishName() {
        return DishName;
    }

    public void setDishName(String dishName) {
        DishName = dishName;
    }

    public String getDishImages() {
        return DishImages;
    }

    public void setDishImages(String dishImages) {
        DishImages = dishImages;
    }

    int DieatId;
    String DishName;
    String DishImages;

}
