package com.example.fitlife.FregmentActivities;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fitlife.Activity.Gym_Exercises_Activity;
import com.example.fitlife.Activity.exercise_detail_activity;
import com.example.fitlife.Models.GymExercisesListModel;
import com.example.fitlife.R;
import com.example.fitlife.adapter.GymExerciseslistAdapter;
import com.example.fitlife.database.Gym_Exerciseslist;

import java.util.ArrayList;


public class FavoriteFregment extends Fragment {

    GymExerciseslistAdapter gymExerciseslistAdapter;

    Gym_Exerciseslist gym_exerciseslist;

    ArrayList<GymExercisesListModel> exerciseslist = new ArrayList<>();



    Intent intent;

    int subcategoryId;
    String subcategoryName;


    CardView pushps;
    RecyclerView activity_favorite_category_recycler_view;

    View v;
    TextView txtnodata;





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        exerciseslist = new ArrayList<>();
        gym_exerciseslist = new Gym_Exerciseslist(getContext());
        exerciseslist = gym_exerciseslist.all_favoriteexercise_list(subcategoryId);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       v= inflater.inflate(R.layout.fragment_favorite_fregment, container, false);






        activity_favorite_category_recycler_view = v.findViewById(R.id.activity_favorite_category_recycler_view);

        pushps =v.findViewById(R.id.pushps);
        txtnodata = v.findViewById(R.id.txtnodata);
        gymExerciseslistAdapter = new GymExerciseslistAdapter(exerciseslist, getContext(), new GymExerciseslistAdapter.ClickListeners() {
            @Override
            public void onViewClick(int position) {


                int tempSubCategoryId = exerciseslist.get(position).getExerciseslistId();
                String tempSubCategoryName = exerciseslist.get(position).getExercisesName();

                Intent intent = new Intent(getActivity(), exercise_detail_activity.class);
                intent.putExtra("ExerciseslistId",tempSubCategoryId);
                intent.putExtra("ExercisesName",tempSubCategoryName);

                startActivity(intent);



            }
        });
        if (exerciseslist.size()<1){
            txtnodata.setVisibility(View.VISIBLE);
            activity_favorite_category_recycler_view.setVisibility(View.GONE);
        }
        else{
            txtnodata.setVisibility(View.GONE);
            activity_favorite_category_recycler_view.setVisibility(View.VISIBLE);

        }
        activity_favorite_category_recycler_view.setLayoutManager(new GridLayoutManager(getContext(),2));
        activity_favorite_category_recycler_view.setAdapter(gymExerciseslistAdapter);



        return v;
    }
}