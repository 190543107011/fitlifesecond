package com.example.fitlife.FregmentActivities;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.fitlife.R;

public class ExerciseFregment extends Fragment {

    ConstraintLayout constraintLayout;
    // ImageView imageview;
    Button button;

    View v;

    int current_image;
    int [] images ={R.drawable.onebody,R.drawable.secondbodys};



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        v= inflater.inflate(R.layout.fragment_exercise_fregment, container, false);


        constraintLayout = v.findViewById(R.id.constraintLayout);
        //imageview =v.findViewById(R.id.imageview);
        button = v.findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                current_image++;
                current_image =current_image % images.length;
                constraintLayout.setBackgroundResource(images[current_image]);
                //imageview.setImageResource(images[current_image]);
            }
        });

return v;
    }

}