package com.example.fitlife.FregmentActivities;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.fitlife.Activity.Bulk_Body_Activity;
import com.example.fitlife.Activity.Dieat_SubCategory_Activity;
import com.example.fitlife.Activity.GymSubCategoryActivity;
import com.example.fitlife.Activity.Lean_Body_Activity;
import com.example.fitlife.Activity.Weight_Gain_Activity;
import com.example.fitlife.Activity.weight__lose_Activity;

import com.example.fitlife.Models.DieatCategoryModel;
import com.example.fitlife.R;

import com.example.fitlife.adapter.DieatCategoryAdapter;
import com.example.fitlife.adapter.GymCategoryAdapter;
import com.example.fitlife.database.DieatCategory;
import com.example.fitlife.database.GymCategory;

import java.util.ArrayList;


public class DieatFregment extends Fragment {


    View v;
    DieatCategoryAdapter dieatCategoryAdapter;

    DieatCategory dieatCategory;

    ArrayList<DieatCategoryModel>dieatlist;

    RecyclerView activity_dieat_category_recycler_view;

    CardView dieatcard;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        dieatlist = new ArrayList<>();
        dieatCategory = new DieatCategory(getContext());
        dieatlist =dieatCategory.all_dieat_list();

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       v= inflater.inflate(R.layout.fragment_dieat_fregment, container, false);

        dieatcard = v.findViewById(R.id.dieatcard);

        activity_dieat_category_recycler_view = v.findViewById(R.id.activity_dieat_category_recycler_view);


      dieatCategoryAdapter = new DieatCategoryAdapter(dieatlist, getContext(), new DieatCategoryAdapter.ClickListeners() {
            @Override
            public void onViewClick(int position) {

                int tempCategoryId = dieatlist.get(position).getDieatId();
                String tempCategoryName = dieatlist.get(position).getDieatName();

                Intent intent;

                intent = new Intent(getActivity(), Dieat_SubCategory_Activity.class);

                intent.putExtra("DieatId",tempCategoryId);
                intent.putExtra("DieatName",tempCategoryName);
                startActivity(intent);


            }
        });
        activity_dieat_category_recycler_view.setLayoutManager(new GridLayoutManager(getContext(),1));
        activity_dieat_category_recycler_view.setAdapter(dieatCategoryAdapter);



        return v;
    }
}