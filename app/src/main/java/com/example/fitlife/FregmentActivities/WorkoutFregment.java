package com.example.fitlife.FregmentActivities;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.fitlife.Activity.Bulk_Body_Activity;
import com.example.fitlife.Activity.GymSubCategoryActivity;
import com.example.fitlife.Activity.Lean_Body_Activity;
import com.example.fitlife.Activity.Weight_Gain_Activity;
import com.example.fitlife.Activity.weight__lose_Activity;
import com.example.fitlife.Models.GymCategoryModel;
import com.example.fitlife.R;
import com.example.fitlife.adapter.GymCategoryAdapter;
import com.example.fitlife.adapter.MyAdapter;
import com.example.fitlife.database.GymCategory;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class WorkoutFregment extends Fragment {

    ViewPager slideimge;
    Timer timer;
    Handler handler;


    View v;

    GymCategoryAdapter gymCategoryAdapter;

    GymCategory gymCategory;

    ArrayList<GymCategoryModel> categorylist;

    RecyclerView activity_gym_category_recycler_view;

    CardView homeworkout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        categorylist = new ArrayList<>();

        gymCategory = new GymCategory(getContext());

        categorylist = gymCategory.all_category_list();



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      v= inflater.inflate(R.layout.fragment_workout_fregment, container, false);

        slideimge = v.findViewById(R.id.slideimge);

        final List<Integer>imagelist = new ArrayList<>();
        imagelist.add(R.drawable.a);
        imagelist.add(R.drawable.b);
        imagelist.add(R.drawable.c);
        imagelist.add(R.drawable.d);


        MyAdapter myAdapter = new MyAdapter(imagelist);
        slideimge.setAdapter(myAdapter);

        handler = new Handler();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        int i = slideimge.getCurrentItem();
                        if (i==imagelist.size()-1)
                        {
                            i=0;
                            slideimge.setCurrentItem(i);
                        }
                        else {
                            i++;
                            slideimge.setCurrentItem(i,true);
                        }

                    }
                });
            }
        },3000,3000);


        homeworkout =v.findViewById(R.id.homeworkout);

      activity_gym_category_recycler_view = v.findViewById(R.id.activity_gym_category_recycler_view);

        gymCategoryAdapter = new GymCategoryAdapter(categorylist, getContext(), new GymCategoryAdapter.ClickListeners() {
            @Override
            public void onViewClick(int position) {
                int tempCategoryId = categorylist.get(position).getCategoryId();
                String tempCategoryName = categorylist.get(position).getCategoryName();

                Intent intent;
                if ("Weight Lose".equalsIgnoreCase(tempCategoryName)) {
                    intent = new Intent(getActivity(), weight__lose_Activity.class);
                }
                else if ("Lean Body".equalsIgnoreCase(tempCategoryName)){
                    intent = new Intent(getActivity(), Lean_Body_Activity.class);
                }
                else if ("Bulk Body".equalsIgnoreCase(tempCategoryName)){
                    intent = new Intent(getActivity(), Bulk_Body_Activity.class);
                }
                else if ("Mass Gainer".equalsIgnoreCase(tempCategoryName)){
                    intent = new Intent(getActivity(), Weight_Gain_Activity.class);
                }

                else  {
                    intent = new Intent(getActivity(), GymSubCategoryActivity.class);
                }

                intent.putExtra("CategoryId",tempCategoryId);
                intent.putExtra("CategoryName",tempCategoryName);
                startActivity(intent);






            }
        });
        activity_gym_category_recycler_view.setLayoutManager(new GridLayoutManager(getContext(),2));
        activity_gym_category_recycler_view.setAdapter(gymCategoryAdapter);

      return  v;
    }











}