package com.example.fitlife.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fitlife.Models.GymExercisesListModel;
import com.example.fitlife.R;
import com.example.fitlife.database.Gym_Exerciseslist;

import java.util.ArrayList;

public class GymExerciseslistAdapter extends RecyclerView.Adapter<GymExerciseslistAdapter.ViewHolder> {

    ArrayList<GymExercisesListModel> exerciseslist;
    Context context;
    ClickListeners clickListeners;

    Gym_Exerciseslist gym_exerciseslist;

    public interface ClickListeners{
        void onViewClick(int position);
    }

    public GymExerciseslistAdapter(ArrayList<GymExercisesListModel> exerciseslist, Context context,ClickListeners onViewClick) {
        this.exerciseslist = exerciseslist;
        this.context = context;
        gym_exerciseslist = new  Gym_Exerciseslist(context);
        clickListeners = onViewClick;
    }
    @NonNull
    @Override
    public GymExerciseslistAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_exercises_category,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull GymExerciseslistAdapter.ViewHolder holder, final int position) {

        holder.row_gymexerciselistid.setText(String.valueOf(exerciseslist.get(position).getExerciseslistId()));
        holder.row_gymexercisename.setText(String.valueOf(exerciseslist.get(position).getExercisesName()));

        int id = context.getResources().getIdentifier("com.example.fitlife:drawable/" + exerciseslist.get(position).getExercisesImage(),null, null);
        holder.row_gymexerciselogo.setImageResource(id);

        holder.pushps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListeners.onViewClick(position);
            }
        });

     if (exerciseslist.get(position).getIsFavorite()==0){
         holder.favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
     }
     else {
         holder.favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
     }
        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gym_exerciseslist.changefavorite(exerciseslist.get(position).getExerciseslistId(),exerciseslist.get(position).getIsFavorite() == 0 ? 1 : 0);
                exerciseslist.get(position).setIsFavorite(exerciseslist.get(position).getIsFavorite() == 0 ? 1 : 0 );
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return exerciseslist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView row_gymexercisename,row_gymexerciselistid;
        ImageView row_gymexerciselogo,favorite;
        CardView pushps;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            row_gymexerciselistid = itemView.findViewById(R.id.row_gymexerciselistid);
            row_gymexercisename = itemView.findViewById(R.id.row_gymexercisename);
            row_gymexerciselogo = itemView.findViewById(R.id.row_gymexerciselogo);
            pushps = itemView.findViewById(R.id.pushps);
            favorite = itemView.findViewById(R.id.favorite);



        }
    }
}

