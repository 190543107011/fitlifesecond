package com.example.fitlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fitlife.Models.DieatCategoryModel;
import com.example.fitlife.R;

import java.util.ArrayList;

public class DieatCategoryAdapter extends RecyclerView.Adapter<DieatCategoryAdapter.ViewHolder> {

    ArrayList<DieatCategoryModel>dieatlist;
    Context context;
   ClickListeners clickListeners;

    public interface ClickListeners{
        void onViewClick(int position);
    }


    public DieatCategoryAdapter(ArrayList<DieatCategoryModel> dieatlist, Context context,ClickListeners onViewClick) {
        this.dieatlist = dieatlist;
        this.context = context;
        clickListeners = onViewClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_dietplans, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.row_dieatid.setText(String.valueOf(dieatlist.get(position).getDieatId()));
        holder.row_dieatcategoryname.setText(String.valueOf(dieatlist.get(position).getDieatName()));

        int id = context.getResources().getIdentifier("com.example.fitlife:drawable/" + dieatlist.get(position).getDieatImages(),null, null);
        holder.row_dieatcategoryimage.setImageResource(id);

        holder.dieatcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListeners.onViewClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dieatlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView dieatcard;
        ImageView row_dieatcategoryimage;
        TextView row_dieatcategoryname,row_dieatid;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            dieatcard = itemView.findViewById(R.id.dieatcard);
            row_dieatid = itemView.findViewById(R.id.row_dieatid);
            row_dieatcategoryname = itemView.findViewById(R.id.row_dieatcategoryname);
            row_dieatcategoryimage = itemView.findViewById(R.id.row_dieatcategoryimage);
        }
    }
}
