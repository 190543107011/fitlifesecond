package com.example.fitlife.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fitlife.Models.DaysCategoryModel;
import com.example.fitlife.Models.GymSubCategoryModel;
import com.example.fitlife.R;

import java.util.ArrayList;

public class Bulk_Body_Adapter extends RecyclerView.Adapter<Bulk_Body_Adapter.ViewHolder> {

    ArrayList<DaysCategoryModel> dayslist1;
    ArrayList<DaysCategoryModel> dayslist2;
    Activity context;
    ArrayList<GymSubCategoryModel> subCategorylist;
    ClickListeners clickListeners;

    public interface ClickListeners {
        void onViewClick(int subcategoryid, String subcategoryname);
    }
    public Bulk_Body_Adapter(ArrayList<GymSubCategoryModel> subCategorylist, ArrayList<DaysCategoryModel> dayslist1, ArrayList<DaysCategoryModel> dayslist2, Activity context, Bulk_Body_Adapter.ClickListeners onViewClick) {
        this.dayslist1 = dayslist1;
        this.dayslist2 = dayslist2;
        this.context = context;
        this.subCategorylist = subCategorylist;
        clickListeners = onViewClick;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Bulk_Body_Adapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_bulk_body, parent, false));
    }


    public void onBindViewHolder(@NonNull final Bulk_Body_Adapter.ViewHolder holder, final int position) {

        holder.dayname.setText(String.valueOf(dayslist1.get(position).getDaysName()));

        int temp1 = 0, temp2 = 0;

        for (int j = 0; j < subCategorylist.size(); j++) {
            if (subCategorylist.get(j).getSubCategoryId() == dayslist1.get(position).getSubCategoryId()) {
                /* subcategoryName = subCategorylist.get(j).getSubCategoryName();*/
                holder.row_gymsubcategoryname.setText(String.valueOf(subCategorylist.get(j).getSubCategoryName()));
                temp1 = j;
            }

            if (subCategorylist.get(j).getSubCategoryId() == dayslist2.get(position).getSubCategoryId()) {
                /* subcategoryName = subCategorylist.get(j).getSubCategoryName();*/
                holder.row_chestname.setText(String.valueOf(subCategorylist.get(j).getSubCategoryName()));
                temp2 = j;
            }

        }


        holder.row_gymdaycategoryid.setText(String.valueOf(dayslist1.get(position).getDaysId()));

        int id = context.getResources().getIdentifier("com.example.fitlife:drawable/" + subCategorylist.get(temp1).getLogo(), null, null);
        holder.row_gymsubcategorylogo.setImageResource(id);

        int id1 = context.getResources().getIdentifier("com.example.fitlife:drawable/" + subCategorylist.get(temp2).getLogo(), null, null);
        holder.row_chestlogo.setImageResource(id1);


        if (clickListeners != null) {
            final int finalTemp = temp1;
            holder.chest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int subcategoryid = subCategorylist.get(finalTemp).getSubCategoryId();
                    String subcategoryname = (String) holder.row_gymsubcategoryname.getText();
                    clickListeners.onViewClick(subcategoryid, subcategoryname);
                }
            });
        }

        if (clickListeners != null) {
            final int finalTemp1 = temp2;
            holder.rowchest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int subcategoryid = subCategorylist.get(finalTemp1).getSubCategoryId();
                    String subcategoryname = (String) holder.row_chestname.getText();
                    clickListeners.onViewClick(subcategoryid, subcategoryname);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return (dayslist1.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView dayname, row_gymsubcategoryname, row_gymdaycategoryid, row_chestname;
        ImageView row_gymsubcategorylogo, row_chestlogo;
        CardView chest, rowchest;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            dayname = itemView.findViewById(R.id.dayname);
            row_gymsubcategoryname = itemView.findViewById(R.id.row_gymsubcategoryname);
            row_gymsubcategorylogo = itemView.findViewById(R.id.row_gymsubcategorylogo);
            chest = itemView.findViewById(R.id.chest);
            row_gymdaycategoryid = itemView.findViewById(R.id.row_gymdaycategoryid);
            row_chestname = itemView.findViewById(R.id.row_chestname);
            row_chestlogo = itemView.findViewById(R.id.row_chestlogo);
            rowchest = itemView.findViewById(R.id.rowchest);

        }
    }
}

