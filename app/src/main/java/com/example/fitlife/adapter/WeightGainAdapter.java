package com.example.fitlife.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fitlife.Activity.weight__lose_Activity;
import com.example.fitlife.Models.GymSubCategoryModel;
import com.example.fitlife.Models.WeightGainModel;
import com.example.fitlife.Models.WeightloseModel;
import com.example.fitlife.R;

import java.util.ArrayList;

public class WeightGainAdapter extends RecyclerView.Adapter<WeightGainAdapter.ViewHolder> {

    ArrayList<WeightGainModel> weekdaylist;
    Activity context;
    ArrayList<GymSubCategoryModel> subCategorylist;

    WeightGainAdapter.ClickListeners clickListeners;

    public interface ClickListeners {
        void onViewClick(int position);
    }

    public WeightGainAdapter(ArrayList<GymSubCategoryModel> subCategorylist, ArrayList<WeightGainModel> weekdaylist, Activity context, WeightGainAdapter.ClickListeners onViewClick) {
        this.weekdaylist = weekdaylist;
        this.context = context;
        this.subCategorylist = subCategorylist;
        clickListeners = onViewClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new WeightGainAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_examples, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        if((position)==0 || position==7 || position==14 || position==21 || position==28){
            holder.row_weeknames.setVisibility(View.VISIBLE);
            holder.row_weeknames.setText(String.valueOf(weekdaylist.get(position).getWeekName()));
        }else{
            holder.row_weeknames.setVisibility(View.GONE);


        }




        for (int j = 0; j < subCategorylist.size(); j++) {
            if (subCategorylist.get(j).getSubCategoryId() == weekdaylist.get(position).getSubCategoryId()) {
                /* subcategoryName = subCategorylist.get(j).getSubCategoryName();*/
                holder.weekday.setText(String.valueOf(subCategorylist.get(j).getSubCategoryName()));
            }
        }


        //holder.row_dayname.setText(String.valueOf(subcategoryName));
        holder.row_weightgain.setText(String.valueOf(weekdaylist.get(position).getWeekId()));

        holder.totalexercises.setText(String.valueOf(weekdaylist.get(position).getTotalExercises()));

        if (clickListeners != null) {
            holder.row_gymtotalexercisename.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListeners.onViewClick(position);
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return weekdaylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView row_weeknames,weekday,totalexercises,row_weightgain;
        CardView row_gymtotalexercisename;
        ImageView weekimage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            row_weeknames = itemView.findViewById(R.id.row_weeknames);
            weekday = itemView.findViewById(R.id.weekday);
            totalexercises = itemView.findViewById(R.id.totalexercises);
            weekimage = itemView.findViewById(R.id.weekimage);
            row_weightgain = itemView.findViewById(R.id.row_weightgain);
            row_gymtotalexercisename = itemView.findViewById(R.id.row_gymtotalexercisename);
        }
    }
}
