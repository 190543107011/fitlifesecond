package com.example.fitlife.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fitlife.Models.DieatSubcategoryModel;
import com.example.fitlife.R;
import com.example.fitlife.database.DieatSubCategory;

import java.util.ArrayList;

public class DieatSubCategoryAdapter extends RecyclerView.Adapter<DieatSubCategoryAdapter.ViewHolder> {

    ArrayList<DieatSubcategoryModel>dieatsubcategorylist;
    Context context;
    DieatSubCategory dieatSubCategory;
   ClickListeners clickListeners;


    public interface ClickListeners{
        void onViewClick(int position);
    }


    public DieatSubCategoryAdapter(ArrayList<DieatSubcategoryModel> dieatsubcategorylist, Context context, ClickListeners onViewClick) {
        this.dieatsubcategorylist = dieatsubcategorylist;
        this.context = context;
        clickListeners = onViewClick;
        dieatSubCategory = new DieatSubCategory(context);


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_dieatsubcategorys, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.row_dishid.setText(String.valueOf(dieatsubcategorylist.get(position).getDishId()));
        holder.row_dieatdishame.setText(String.valueOf(dieatsubcategorylist.get(position).getDishName()));

        holder.row_dieatkcal.setText(String.valueOf(dieatsubcategorylist.get(position).getDishKcal()));

        int id = context.getResources().getIdentifier("com.example.fitlife:drawable/" + dieatsubcategorylist.get(position).getDishImages(),null, null);
        holder.row_dishimages.setImageResource(id);


        holder.dieatsubcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListeners.onViewClick(position);
            }
        });

        if (dieatsubcategorylist.get(position).getIsFavorite()==0){
            holder.favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
        }
        else {
            holder.favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
        }
        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dieatSubCategory.changefavorite(dieatsubcategorylist.get(position).getDieatId(),dieatsubcategorylist.get(position).getIsFavorite() == 0 ? 1 : 0);
                dieatsubcategorylist.get(position).setIsFavorite(dieatsubcategorylist.get(position).getIsFavorite() == 0 ? 1 : 0 );
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return dieatsubcategorylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView dieatsubcategory;
        ImageView row_dishimages,favorite;
        TextView row_dieatdishame,row_dishid,row_dieatkcal;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            dieatsubcategory = itemView.findViewById(R.id.dieatsubcategory);
            row_dishimages = itemView.findViewById(R.id.row_dishimages);
            row_dieatdishame = itemView.findViewById(R.id.row_dieatdishame);
            row_dishid = itemView.findViewById(R.id.row_dishid);
            row_dieatkcal = itemView.findViewById(R.id.row_dieatkcal);
            favorite =itemView.findViewById(R.id.favorite);
        }
    }
}

