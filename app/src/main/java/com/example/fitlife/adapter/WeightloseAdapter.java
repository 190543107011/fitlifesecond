package com.example.fitlife.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fitlife.Models.GymSubCategoryModel;
import com.example.fitlife.Models.WeightloseModel;
import com.example.fitlife.R;

import java.util.ArrayList;

public class WeightloseAdapter extends RecyclerView.Adapter<WeightloseAdapter.ViewHolder> {
    ArrayList<WeightloseModel> weeklist;
    Activity context;
    ArrayList<GymSubCategoryModel> subCategorylist;

    ClickListeners clickListeners;

    public interface ClickListeners {
        void onViewClick(int position);
    }

    public WeightloseAdapter(ArrayList<GymSubCategoryModel> subCategorylist, ArrayList<WeightloseModel> weeklist, Activity context, ClickListeners onViewClick) {
        this.weeklist = weeklist;
        this.context = context;
        clickListeners = onViewClick;
        this.subCategorylist = subCategorylist;

    }


    @NonNull
    @Override
    public WeightloseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_weightlose, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull WeightloseAdapter.ViewHolder holder, final int position) {

        String subcategoryName = "";

        if((position)==0 || position==7 || position==14 || position==21 || position==28){
            holder.row_weekname.setVisibility(View.VISIBLE);
            holder.row_weekname.setText(String.valueOf(weeklist.get(position).getWeekName()));
        }else{
            holder.row_weekname.setVisibility(View.GONE);
        }



        for (int j = 0; j < subCategorylist.size(); j++) {
            if (subCategorylist.get(j).getSubCategoryId() == weeklist.get(position).getSubCategoryId()) {
                /* subcategoryName = subCategorylist.get(j).getSubCategoryName();*/
                holder.row_dayname.setText(String.valueOf(subCategorylist.get(j).getSubCategoryName()));
            }
        }


        //holder.row_dayname.setText(String.valueOf(subcategoryName));
        holder.row_weightlose.setText(String.valueOf(weeklist.get(position).getWeekId()));

        if (clickListeners != null) {
            holder.row_gymdayname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListeners.onViewClick(position);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return weeklist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView row_weekname, row_dayname, row_weightlose;
        CardView row_gymdayname;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            row_weekname = itemView.findViewById(R.id.row_weekname);
            row_dayname = itemView.findViewById(R.id.row_dayname);
            row_weightlose = itemView.findViewById(R.id.row_weightlose);
            row_gymdayname = itemView.findViewById(R.id.row_gymdayname);

        }
    }
}

