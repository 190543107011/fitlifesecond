package com.example.fitlife;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.fitlife.FregmentActivities.DieatFregment;
import com.example.fitlife.FregmentActivities.ExerciseFregment;
import com.example.fitlife.FregmentActivities.FavoriteFregment;
import com.example.fitlife.FregmentActivities.WorkoutFregment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class FregmentActivity extends AppCompatActivity {

    BottomNavigationView bottomnav;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fregment);

        /*toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);*/

        bottomnav = findViewById(R.id.bottomnav);

        bottomnav.setOnNavigationItemSelectedListener(bottomNavMethod);

        getSupportFragmentManager().beginTransaction().replace(R.id.container,new ExerciseFregment()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavMethod = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

            Fragment fragment = null;
            switch (menuItem.getItemId())
            {

                case R.id.nav_workout:
                    fragment = new WorkoutFregment();
                    setTitle("WorkOut");
                    break;
                case R.id.nav_exercise:
                    fragment = new ExerciseFregment();
                    setTitle("Exercise");
                    break;

                case R.id.nav_Favorite:
                    fragment = new FavoriteFregment();

                    break;
                case R.id.nav_dieate:
                    fragment = new DieatFregment();

                    break;

            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();

            return false;
        }
    };
}