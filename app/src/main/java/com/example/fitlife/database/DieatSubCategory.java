package com.example.fitlife.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.fitlife.Models.DieatSubcategoryModel;

import java.util.ArrayList;

public class DieatSubCategory extends Mydatabase {

    public static final String DieatPlanDishes="DieatPlanDishes";
    public static final String DishId="DishId";
    public static final String DishName ="DishName";
    public static final String DishImages ="DishImages";
    public static final String DieatId ="DieatId";
    public static final String IS_FAVORITE = "IsFavorite";
    public static final String DishKcal = "DishKcal";

    public ArrayList<DieatSubcategoryModel>all_dieatsubcategory_list(int id){

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<DieatSubcategoryModel>dieatsubcategorylist = new ArrayList<>();

        String query = "select * from " + DieatPlanDishes+" where "+DieatId+"="+id;


        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        do{
            DieatSubcategoryModel dieatSubcategoryModel = new DieatSubcategoryModel();
            dieatSubcategoryModel.setDishId(cursor.getInt(cursor.getColumnIndex(DishId)));
            dieatSubcategoryModel.setDishName(cursor.getString(cursor.getColumnIndex(DishName)));
            dieatSubcategoryModel.setDishImages(cursor.getString(cursor.getColumnIndex(DishImages)));
            dieatSubcategoryModel.setDieatId(cursor.getInt(cursor.getColumnIndex(DieatId)));
            dieatSubcategoryModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
            dieatSubcategoryModel.setDishKcal(cursor.getString(cursor.getColumnIndex(DishKcal)));

            dieatsubcategorylist.add(dieatSubcategoryModel);

        }while (cursor.moveToNext());

        db.close();
        cursor.close();
        return dieatsubcategorylist;
    }

    public void changefavorite(int ExerciseslistId, int favoritevalue) {

        SQLiteDatabase db = getWritableDatabase();

        String query = "Update " + DieatPlanDishes + " SET " + IS_FAVORITE + " = " + favoritevalue + " where " + DishId + " = " + ExerciseslistId;

        db.execSQL(query);
        db.close();

    }

    public ArrayList<DieatSubcategoryModel>all_favoriteexercise_list(int id){

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<DieatSubcategoryModel>dieatsubcategorylist = new ArrayList<>();

        String query = "select * from " + DieatPlanDishes + " where " + IS_FAVORITE+" = 1";


        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        do{
            DieatSubcategoryModel dieatSubcategoryModel = new DieatSubcategoryModel();
            dieatSubcategoryModel.setDishId(cursor.getInt(cursor.getColumnIndex(DishId)));
            dieatSubcategoryModel.setDishName(cursor.getString(cursor.getColumnIndex(DishName)));
            dieatSubcategoryModel.setDishImages(cursor.getString(cursor.getColumnIndex(DishImages)));
            dieatSubcategoryModel.setDieatId(cursor.getInt(cursor.getColumnIndex(DieatId)));
            dieatSubcategoryModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
            dieatSubcategoryModel.setDishKcal(cursor.getString(cursor.getColumnIndex(DishKcal)));

            dieatsubcategorylist.add(dieatSubcategoryModel);

        }while (cursor.moveToNext());

        db.close();
        cursor.close();
        return dieatsubcategorylist;
    }


    public DieatSubCategory(Context context) {
        super(context);
    }
}
