package com.example.fitlife.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.fitlife.Models.GymExercisesListModel;

import java.util.ArrayList;

public class Gym_Exerciseslist extends Mydatabase {

    public static final String TBL_GYM_EXERCISE_LIST = "GymExercisesList";//GYMEXERCISELIST
    public static final String EXERCISESLIST_ID = "ExerciseslistId";
    public static final String EXERCISES_NAME = "ExercisesName";
    public static final String EXERCISES_IMAGE = "ExercisesImage";
    public static final String CATEGORY_ID = "CategoryId";
    public static final String SUB_CATEGORY_ID = "SubCategoryId";
    public static final String IS_FAVORITE = "IsFavorite";

    public ArrayList<GymExercisesListModel> all_exercise_list(int subcategoryId) {

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<GymExercisesListModel> exerciseslist = new ArrayList<>();

        String query = "select * from " + TBL_GYM_EXERCISE_LIST + " where " + SUB_CATEGORY_ID + "=" + subcategoryId;

        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        do {
            GymExercisesListModel gymExercisesListModel = new GymExercisesListModel();
            gymExercisesListModel.setExerciseslistId(cursor.getInt(cursor.getColumnIndex(EXERCISESLIST_ID)));
            gymExercisesListModel.setExercisesName(cursor.getString(cursor.getColumnIndex(EXERCISES_NAME)));
            gymExercisesListModel.setExercisesImage(cursor.getString(cursor.getColumnIndex(EXERCISES_IMAGE)));
            gymExercisesListModel.setSubCategoryId(cursor.getInt(cursor.getColumnIndex(SUB_CATEGORY_ID)));
            gymExercisesListModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));


            exerciseslist.add(gymExercisesListModel);
        } while (cursor.moveToNext());

        db.close();
        cursor.close();
        return exerciseslist;
    }

    public void changefavorite(int ExerciseslistId, int favoritevalue) {
        SQLiteDatabase db = getWritableDatabase();

        String query = "Update " + TBL_GYM_EXERCISE_LIST + " SET " + IS_FAVORITE + " = " + favoritevalue + " where " + EXERCISESLIST_ID + " = " + ExerciseslistId;

        //db.update(GymExercisesList,cv,this.ExerciseslistId + " = ?", new Integer[]{ExerciseslistId});

        db.execSQL(query);
        db.close();
    }

    public ArrayList<GymExercisesListModel> all_favoriteexercise_list(int subcategoryId) {

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<GymExercisesListModel> exerciseslist = new ArrayList<>();

        String query = "select * from " + TBL_GYM_EXERCISE_LIST + " where " + IS_FAVORITE+" = 1";

        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();

        if (cursor.getCount()>0) {
            do {
                GymExercisesListModel gymExercisesListModel = new GymExercisesListModel();
                gymExercisesListModel.setExerciseslistId(cursor.getInt(cursor.getColumnIndex(EXERCISESLIST_ID)));
                gymExercisesListModel.setExercisesName(cursor.getString(cursor.getColumnIndex(EXERCISES_NAME)));
                gymExercisesListModel.setExercisesImage(cursor.getString(cursor.getColumnIndex(EXERCISES_IMAGE)));
                gymExercisesListModel.setSubCategoryId(cursor.getInt(cursor.getColumnIndex(SUB_CATEGORY_ID)));
                gymExercisesListModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));


                exerciseslist.add(gymExercisesListModel);
            } while (cursor.moveToNext());
        }
        db.close();
        cursor.close();
        return exerciseslist;
    }


    public Gym_Exerciseslist(Context context) {
        super(context);
    }
}
