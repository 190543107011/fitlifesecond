package com.example.fitlife.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.fitlife.Models.WeightloseModel;
import com.example.fitlife.database.Mydatabase;

import java.util.ArrayList;

public class Weightlose extends Mydatabase {
    public static  final String Weightlose = "Weightlose";
    public static  final String WeekId = "WeekId";
    public static  final String WeekName = "WeekName";
    public static  final String SubCategoryId = "SubCategoryId";
    public static  final String CategoryId = "CategoryId";


    public ArrayList<WeightloseModel> all_week_name(int id){

        SQLiteDatabase db = getReadableDatabase();

        ArrayList<WeightloseModel>weeklist = new ArrayList<>();

        String query = "select * from " + Weightlose +" where "+CategoryId+"="+id;

        Cursor cursor = db.rawQuery(query,null);

        cursor.moveToFirst();
        do{
            WeightloseModel weightloseModel = new WeightloseModel();
            weightloseModel.setWeekId(cursor.getInt(cursor.getColumnIndex(WeekId)));
            weightloseModel.setWeekName(cursor.getString(cursor.getColumnIndex(WeekName)));
            weightloseModel.setSubCategoryId(cursor.getInt(cursor.getColumnIndex(SubCategoryId)));
            weightloseModel.setCategoryId(cursor.getInt(cursor.getColumnIndex(SubCategoryId)));

            weeklist.add(weightloseModel);

        }while (cursor.moveToNext());

        db.close();
        cursor.close();
        return weeklist;
    }

    public Weightlose(Context context) {
        super(context);
    }
}

