package com.example.fitlife.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.fitlife.Models.DaysCategoryModel;

import java.util.ArrayList;

public class DaysCategorys extends Mydatabase {

    public static final String DaysCategory = "DaysCategory";
    public static final String DaysId = "DaysId";
    public static final String DaysName = "DaysName";
    public static final String SubCategoryId = "SubCategoryId";
    public static final String CategoryId = "CategoryId";

    public ArrayList<DaysCategoryModel> all_day_category(int id){

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<DaysCategoryModel>dayslist = new ArrayList<>();

        String query = "select * from " + DaysCategory+" where "+CategoryId+"="+id;

        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        do{
            DaysCategoryModel daysCategoryModel = new DaysCategoryModel();
            daysCategoryModel.setDaysId(cursor.getInt(cursor.getColumnIndex(DaysId)));
            daysCategoryModel.setDaysName(cursor.getString(cursor.getColumnIndex(DaysName)));
            daysCategoryModel.setSubCategoryId(cursor.getInt(cursor.getColumnIndex(SubCategoryId)));
            daysCategoryModel.setCategoryId(cursor.getInt(cursor.getColumnIndex(CategoryId)));

            dayslist.add(daysCategoryModel);

        }while (cursor.moveToNext());

        db.close();
        cursor.close();
        return dayslist;

    }

    public DaysCategorys(Context context) {
        super(context);
    }
}
