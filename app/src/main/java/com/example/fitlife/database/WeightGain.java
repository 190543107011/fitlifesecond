package com.example.fitlife.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.fitlife.Models.WeightGainModel;
import com.example.fitlife.Models.WeightloseModel;

import java.util.ArrayList;

public class WeightGain extends Mydatabase {

    public static  final String WEIGHTGAIN = "WeightGain";
    public static  final String WEEKID = "WeekId";
    public static  final String WEEKNAME = "WeekName";
    public static  final String SUBCATEGORYID = "SubCategoryId";
    public static  final String CATEGORYID = "CategoryId";
    public static  final String TOTALEXERCISES = "TotalExercises";


    public ArrayList<WeightGainModel> all_gain_exercises(int id){

        SQLiteDatabase db = getReadableDatabase();

        ArrayList<WeightGainModel>weekdaylist = new ArrayList<>();

        String query = "select * from " + WEIGHTGAIN +" where "+CATEGORYID+"="+id;

        Cursor cursor = db.rawQuery(query,null);

        cursor.moveToFirst();
        do{
           WeightGainModel weightGainModel = new WeightGainModel();
            weightGainModel.setWeekId(cursor.getInt(cursor.getColumnIndex(WEEKID)));
            weightGainModel.setWeekName(cursor.getString(cursor.getColumnIndex(WEEKNAME)));
            weightGainModel.setSubCategoryId(cursor.getInt(cursor.getColumnIndex(SUBCATEGORYID)));
            weightGainModel.setCategoryId(cursor.getInt(cursor.getColumnIndex(CATEGORYID)));
            weightGainModel.setTotalExercises(cursor.getString(cursor.getColumnIndex(TOTALEXERCISES)));
            weekdaylist.add(weightGainModel);

        }while (cursor.moveToNext());

        db.close();
        cursor.close();
        return weekdaylist;
    }




    public WeightGain(Context context) {
        super(context);
    }
}
