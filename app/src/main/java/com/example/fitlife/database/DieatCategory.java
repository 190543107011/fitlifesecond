package com.example.fitlife.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.fitlife.Models.DieatCategoryModel;

import java.util.ArrayList;

public class DieatCategory extends Mydatabase {

    public static final String DieatPlans = "DieatPlans";
    public static final String DieatId = "DieatId";
    public static final String DieatName = "DieatName";
    public static final String DieatImages = "DieatImages";

    public ArrayList<DieatCategoryModel>all_dieat_list(){

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<DieatCategoryModel>dieatlist = new ArrayList<>();

        String query = "select * from " + DieatPlans;
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        do {

            DieatCategoryModel dieatCategoryModel = new DieatCategoryModel();
            dieatCategoryModel.setDieatId(cursor.getInt(cursor.getColumnIndex(DieatId)));
            dieatCategoryModel.setDieatName(cursor.getString(cursor.getColumnIndex(DieatName)));
            dieatCategoryModel.setDieatImages(cursor.getString(cursor.getColumnIndex(DieatImages)));

            dieatlist.add(dieatCategoryModel);

        }while (cursor.moveToNext());
        db.close();
        cursor.close();
        return dieatlist;

    }


    public DieatCategory(Context context) {
        super(context);
    }
}
